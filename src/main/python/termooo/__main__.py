from selenium import webdriver
from termooo.selenium import TermoooDriver
from termooo.web import TermoooPage
import argparse


def main(words: int):
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-extensions")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument("--start-maximized")
    driver = webdriver.Chrome(options=options,)
    driver.set_window_size(1920,1080)
    termooo = TermoooDriver(driver)
    result = termooo.run(TermoooPage.create(words))
    print(" ".join(result))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'words',
        choices=(1, 2, 4,),
        type=int,
    )
    namespace = parser.parse_args()
    main(**vars(namespace))
