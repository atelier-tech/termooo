from dataclasses import dataclass

from termooo import Feedback


base_url = 'https://term.ooo'

classes_map = {
    'wrong': Feedback.MISS,
    'place': Feedback.WRONG_PLACE,
    'right' : Feedback.RIGHT_PLACE,
}

def class_to_feedback(classes):
    classes = set(classes.split())
    for k,v in classes_map.items():
        if k in classes:
            return v
    raise Exception(f'wrong classes {classes}')



storage_keys = {
    1 : 'termo',
    2 : 'duo',
    4 : 'quatro',
}


@dataclass
class TermoooPage():

    url: str
    words: int
    storage_key: str

    @classmethod
    def to_url(cls, words=1) -> str:
        if words not in (1, 2, 4,):
            raise Exception(f'Termooo doesn\'t support words = {words} ')
        if words == 1:
            return base_url
        return base_url + f'/{words}/'

    @classmethod
    def create(cls, words=1):
        return cls(
            url=cls.to_url(words),
            words=words,
            storage_key=storage_keys.get(words)
        )





