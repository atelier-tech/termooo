import csv
import os
from cmath import inf
from enum import Enum
import itertools
from pathlib import Path
import unicodedata
import re

ALL_LETTERS = 'abcdefghijklmnopqrstuvwxyz'

RESOURCES = Path(os.getenv('RESOURCES', './'))

WORDS_FILES = (
    'fserb/pt-br/conjugações',
    'fserb/pt-br/dicio',
    'fserb/pt-br/verbos',
    'fserb/pt-br/palavras',
)

class Feedback(Enum):
    MISS = 1
    RIGHT_PLACE = 2
    WRONG_PLACE = 3


def strip_accents(text):
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode('utf-8')
    return str(text)


def remove_miss(letter, words):
    return [word for word in words if letter not in word]


def remove_right_place(index, letter, words):
    return [word for word in words if letter == word[index]]


def remove_wrong_place(index, letter, words):
    return [word for word in words if letter in word and letter != word[index]]


def has_any(letters, count=1):
    def fn(word):
        counter = 0
        for c in word:
            if c in letters:
                counter += 1
        return counter >= count
    return fn


def load_data(*files, base=RESOURCES):
    for file in files:
        file = base.joinpath(file)
        with open(file) as input_stream:
            reader = csv.DictReader(input_stream, delimiter=',')
            yield from reader

def load_word_frequency(data = None):
    data = data or load_data('fserb/pt-br/data')
    size_5 = lambda it: len(it['palavra']) == 5
    tf_gt_0 = lambda it: int(it['tf']) > 0
    data = filter(size_5, data)
    data = filter(tf_gt_0, data)
    frequency = {}
    for item in data:
        word = strip_accents(item['palavra'])
        frequency[word] = frequency.get(word, 0) + int(item['tf'])
    return frequency

def load_words(*files, base=RESOURCES):
    for file in files:
        file = base.joinpath(file)
        is_ok = re.compile('^[a-z]{5}$')
        with open(file, 'r', encoding='utf-8') as stream:
            words = map(str.strip, stream.readlines())
            words = map(str.lower, words)
            words = map(strip_accents, words)
            words = filter(lambda word: is_ok.match(word), words)
            words = filter(has_any('aeiou', 2), words)
            yield from list(words)


def calc_letter_frequency(words):
    letter_frequency = dict(
        (letter, 0) for letter in ALL_LETTERS
    )
    for letter in letter_frequency.keys():
        for word in words:
            if letter in word:
                letter_frequency[letter] = letter_frequency.get(letter, 0) + 1
    return letter_frequency


def letter_frequency(words, vocabulary, avoid = ()):
    ranking = dict((word, 0) for word in words)
    letter_frequency = calc_letter_frequency(vocabulary)
    for word in words:
        points = 0
        for letter, frequency in letter_frequency.items():
            if letter in avoid:
                continue
            if letter in word:
                points += frequency
        ranking[word] = points
    return ranking

class ImpactEvaluator():

    def __init__(self, words, word_frequency = None) -> None:
        self.words = words
        self.cache = {}
        self.word_frequency = word_frequency or {}

    def move_impact(self, move):
        if move in self.cache:
            return self.cache[move]
        hit, place, miss = 0, 0, 0
        for word in self.words:
            index, letter = move
            if word[index] == letter:
                hit += 1
            if letter in word:
                place += 1
            else:
                miss += 1
        hit = (hit / len(self.words)) * (len(self.words) - hit)
        place =  (place / len(self.words)) * (len(self.words) - place)
        miss = ((len(self.words) - miss) / len(self.words)) * miss
        impact = (
            min((hit, place, miss,)),
            sum((hit, place, miss,)),
        )
        self.cache[move] = impact
        return impact


    def sum(self, impacts):
        return tuple(map(sum, zip(*impacts)))

    def word_impact(self, word):
        m,s = self.sum(map(self.move_impact, enumerate(word)))
        return (m, 1 if word in self.words else 0, self.word_frequency.get(word,0), s)

    def evaluate(self, word):
        return self.word_impact(word)

class Termooo():

    def __init__(self, words, word_frequency = None):
        self.words = list(map(str.lower, words))
        self.vocabulary = list(self.words)
        self.contains = set()
        self.used_letters = set()
        self.word_frequency = word_frequency or {}

    def absent_word(self, word):
        try:
            self.vocabulary.remove(word)
            self.words.remove(word)
        except ValueError:
            pass

    def evaluate(self, word, evaluator):
        if len(self.words) == 1 and self.words[0] == word:
            return (inf, 0, 0)
        evaluator = evaluator or ImpactEvaluator(self.words, self.word_frequency)
        return evaluator.evaluate(word)

    def next_word(self):
        if len(self.words) == 1:
            return self.words[0]
        evaluator = ImpactEvaluator(self.words, self.word_frequency)
        words_with_points = dict([
            (word, evaluator.evaluate(word), )
            for word in self.vocabulary
        ])
        word = max(list(words_with_points.items()), key=lambda it: it[1])[0]
        return word

    def feedback(self, word_feedback):
        previous_words = self.words
        word = ''.join(map(lambda it: it[0], word_feedback))
        try:
            self.vocabulary.remove(word)
        except ValueError:
            pass
        for index, feed in enumerate(word_feedback):
            letter, feedback = feed
            if feedback == Feedback.MISS:
                continue
            self.contains.add(letter)
        for index, feed in enumerate(word_feedback):
            letter, feedback = feed
            letter = letter.lower()
            self.used_letters.add(letter)
            if feedback == Feedback.MISS:
                if letter not in self.contains:
                    self.words = remove_miss(letter, self.words)
                else:
                    self.words = remove_wrong_place(index, letter, self.words)
            if feedback == Feedback.RIGHT_PLACE:
                self.words = remove_right_place(index, letter, self.words)
            if feedback == Feedback.WRONG_PLACE:
                self.words = remove_wrong_place(index, letter, self.words)
        if len(self.words) == 0:
            raise Exception(
                f'no words to match\nprevious words: {previous_words}\nfeedback was: {word_feedback}')


class MultipleTermooo():

    @classmethod
    def create(cls, times: int, words, word_frequency):
        if times == 1:
            return Termooo(words, word_frequency)
        return cls(*[Termooo(list(words), word_frequency) for _ in range(times)])

    def __init__(self, *termooo: Termooo) -> None:
        self.termooo_list = list(termooo)
        self.used_words = set()

    @property
    def words(self):
        return list(itertools.chain(*[ termooo.words for termooo in self.termooo_list]))

    def feedback(self, *words):
        for termooo, word_feedback in zip(self.termooo_list, words):
            word_feedback = list(word_feedback)
            if len(termooo.words) > 1:
                termooo.feedback(word_feedback)

    def evaluate(self, word, evaluators):
        evaluators = evaluators or [ ImpactEvaluator(termooo.words, termooo.word_frequency) for termooo in self.termooo_list]
        evaluations = [ evaluator.evaluate(word) for evaluator in evaluators ]
        return (tuple(map(sum, zip(*evaluations))),) + tuple(sorted(evaluations, reverse=False))

    def next_word(self):
        evaluators = [ ImpactEvaluator(termooo.words, termooo.word_frequency) for termooo in self.termooo_list]
        vocabulary = set(itertools.chain(*[termooo.vocabulary for termooo in self.termooo_list if len(termooo.words) > 1]))
        words = list([ termooo.words[0] for termooo in self.termooo_list if len(termooo.words) == 1])

        def evaluate(word):
            return self.evaluate(word, evaluators)

        if len(vocabulary) > 0:
            words += [max(vocabulary, key=evaluate)]

        for word in words:
            if word in self.used_words:
                continue
            self.used_words.add(word)
            return word
        raise StopIteration()

    def absent_word(self, word):
        for termooo in self.termooo_list:
            termooo.absent_word(word)
