from random import shuffle
import time
import sys
import traceback
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from termooo import WORDS_FILES, MultipleTermooo, load_word_frequency, load_words
import json

from termooo.web import TermoooPage, class_to_feedback


class Clipboard:

    def __init__(self, driver) -> None:
        self.driver = driver

    def get_content(self):
        self.driver.execute_script("return window.navigator.clipboard.readText()")

class LocalStorage:

    def __init__(self, driver):
        self.driver = driver

    def __len__(self):
        return self.driver.execute_script("return window.localStorage.length;")

    def items(self):
        return self.driver.execute_script(
            "var ls = window.localStorage, items = {}; "
            "for (var i = 0, k; i < ls.length; ++i) "
            "  items[k = ls.key(i)] = ls.getItem(k); "
            "return items; ")

    def keys(self):
        return self.driver.execute_script(
            "var ls = window.localStorage, keys = []; "
            "for (var i = 0; i < ls.length; ++i) "
            "  keys[i] = ls.key(i); "
            "return keys; ")

    def get(self, key):
        return json.loads(
            self.driver.execute_script(
                "return window.localStorage.getItem(arguments[0]);",
                key
            )
        )

    def set(self, key, value):
        self.driver.execute_script(
            "window.localStorage.setItem(arguments[0], arguments[1]);",
            key, value
        )

    def has(self, key):
        return key in self.keys()

    def remove(self, key):
        self.driver.execute_script(
            "window.localStorage.removeItem(arguments[0]);", key)

    def clear(self):
        self.driver.execute_script("window.localStorage.clear();")

    def __getitem__(self, key):
        value = self.get(key)
        if value is None:
            raise KeyError(key)
        return value

    def __setitem__(self, key, value):
        self.set(key, value)

    def __contains__(self, key):
        return key in self.keys()

    def __iter__(self):
        return self.items().__iter__()

    def __repr__(self):
        return self.items().__str__()


def expand_shadow_element(driver, element):
    shadow_root = driver.execute_script(
        'return arguments[0].shadowRoot',
        element,
    )
    return shadow_root


class TermoooDriver():

    def __init__(self, driver) -> None:
        self.wait = WebDriverWait(driver, 10)
        self.driver = driver
        self.local_storage = LocalStorage(self.driver)
        self.clipboard = Clipboard(self.driver)

    def wait_page_load(self):
        self.wait.until(EC.presence_of_element_located((
            By.CSS_SELECTOR,
            "main wc-board#board0",
        )))

    def wait_feedback(self):
        time.sleep(2)

    def was_accepted(self):
        notify_message = self.get_notify_message()
        return not 'não é aceita' in notify_message.text

    def get_body(self):
        return self.driver.find_element(By.CSS_SELECTOR, 'body')

    def get_modal(self):
        body = self.get_body()
        return body.find_element(By.CSS_SELECTOR, 'wc-modal')

    def get_stats(self):
        modal = self.get_modal()
        return expand_shadow_element(self.driver, modal.find_element(By.CSS_SELECTOR, 'wc-stats'))

    def get_share_button(self):
        stats = self.get_stats()
        return stats.find_element(By.ID, 'stats_share')

    def get_notify_message(self):
        return self.get_notify().find_element(By.ID, 'msg')

    def get_notify(self):
        body = self.get_body()
        return expand_shadow_element(self.driver, body.find_element(By.CSS_SELECTOR, 'wc-notify'))

    def get_board(self, index=0):
        body = self.get_body()
        return expand_shadow_element(self.driver, body.find_element(By.CSS_SELECTOR, f'main wc-board#board{index}'))

    def get_line(self, board, index):
        return expand_shadow_element(self.driver, board.find_element(By.CSS_SELECTOR, f'[termo-row="{index}"]'))

    def is_finished(self, page: TermoooPage) -> bool:
        if page.storage_key not in self.local_storage:
            return False
        return self.local_storage[page.storage_key]['meta']['endTime'] != 0

    def clear(self):
        body = self.get_body()
        for _ in range(5):
            body.send_keys(Keys.BACKSPACE)

    def as_feedback(self, word, line):
        items = line.find_elements(By.CSS_SELECTOR, '.letter')
        for c, item in zip(word, items):
            yield c, class_to_feedback(item.get_attribute('class'))

    def collect_feedback(self, page: TermoooPage, word: str, attempt: int):
        for board in range(page.words):
            board = self.get_board(board)
            line = self.get_line(board, attempt)
            try:
                feedback = tuple(self.as_feedback(word, line))
                yield feedback
            except:
                yield ()

    def run(self, page: TermoooPage):
        try:
            self.driver.get(page.url)
            self.wait_page_load()
            body = self.get_body()
            body.send_keys(Keys.ESCAPE)
            vocabulary = sorted(list(set(load_words(*WORDS_FILES))))
            word_frequency = load_word_frequency()
            termooo = MultipleTermooo.create(page.words, vocabulary, word_frequency)
            attempt = 0
            while not self.is_finished(page):
                try:
                    current_word = termooo.next_word()
                except StopIteration:
                    continue
                body.send_keys(current_word)
                body.send_keys(Keys.RETURN)
                self.wait_feedback()
                if self.is_finished(page):
                    break
                if not self.was_accepted():
                    termooo.absent_word(current_word)
                    self.clear()
                    continue
                feedback = tuple(
                    self.collect_feedback(page, current_word, attempt)
                )
                termooo.feedback(*feedback)
                attempt += 1
            self.driver.save_screenshot("output.png")
            self.get_share_button().click()
            body.send_keys(Keys.ESCAPE)
            self.driver.save_screenshot("result.png")
            #content = self.clipboard.get_content()
            return termooo.words
        except Exception as ex:
            print(ex)
            traceback.print_exc(file=sys.stdout)
            self.driver.save_screenshot("error.png")
