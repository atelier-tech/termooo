from unittest import TestCase

from selenium import webdriver
from termooo.selenium import TermoooDriver
from termooo.web import TermoooPage


class SeleniumTest(TestCase):

    def test_quatro(self):
        driver = webdriver.Chrome()
        termooo = TermoooDriver(driver)
        termooo.run(TermoooPage.create(4))

    def test_term(self):
        driver = webdriver.Chrome()
        termooo = TermoooDriver(driver)
        termooo.run(TermoooPage.create(1))

    def test_duo(self):
        driver = webdriver.Chrome()
        termooo = TermoooDriver(driver)
        termooo.run(TermoooPage.create(2))
